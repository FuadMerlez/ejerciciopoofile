﻿using CrudModel.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace CrudModel.DAL
{
    public class PersonaDalObjeto:IPersonaDAL  // Objetos =  API  = Json  =TXT..CSV = SQL
    {
        private static List<Persona> arrPersonas = new List<Persona>();
        public void agregarPersona(Persona p)
        {
            arrPersonas.Add(p);
        }
        public List<Persona> listarPersonas()
        {
            return arrPersonas;
        }
        public List<Persona> BuscarNombre(String nombre)
        {
            return arrPersonas.FindAll(p => p.Nombre1.Equals(nombre));

        }

        public void EliminarPersona(int index)
        {
            throw new NotImplementedException();
        }

        public void actualizarPersona(Persona p)
        {
            throw new NotImplementedException();
        }
    }
}

