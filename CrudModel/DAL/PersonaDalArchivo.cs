﻿using CrudModel.DTO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace CrudModel.DAL
{
    public class PersonaDalArchivo : IPersonaDAL
    {

        private static String archivo = "personas.txt";
        private static String ruta = Directory.GetCurrentDirectory() + "/" + archivo;

        public void actualizarPersona(Persona p)
        {
            List<Persona> lpersona = listarPersonas();
            File.Delete(ruta);

            foreach (var pe in lpersona){

                if (pe.Identificador != p.Identificador)
                {

                    new Persona()
                    {
                        Identificador = pe.Identificador,
                        Nombre1 = pe.Nombre1,
                        FechaNacimiento = pe.FechaNacimiento,
                        Rut = pe.Rut

                    };
                }
                else {
                    new Persona()
                    {
                        Identificador = p.Identificador,
                        Nombre1 = p.Nombre1,
                        FechaNacimiento = p.FechaNacimiento,
                        Rut = p.Rut

                    };
                }
            }

        }

        public void agregarPersona(Persona p)
        {
            //Crear el StreamWrite
            //Agregr la info al archivo
            // Cerrar el StreamWrite

            try
            {

                using (StreamWriter writer = new StreamWriter(ruta, true)) 
                {
                    //== Crear el archivo y escribirlo
                    String texto = p.Nombre1 + ";"
                            + p.Rut + ";"
                            + p.FechaNacimiento + ";"
                            + p.Identificador 
                            ;
                    //CSV =:Comma Separated Values 

                    writer.WriteLine(texto);
                    writer.Flush();




                }


            }catch(Exception e)
            {
                Console.WriteLine("Error al escribir el archivo " + e.Message);
            }

           
        }

        public List<Persona> BuscarNombre(string nombre)
        {
            List<Persona> persona = new List<Persona>();
            return persona;
        }

        public void EliminarPersona(int index)
        {

            List<Persona>lPrsona= listarPersonas();

            File.Delete(ruta);

            foreach (var pe in lPrsona){
                
                if(index!=pe.Identificador){
                    Persona pnew = new Persona()
                    {
                        Identificador = pe.Identificador,
                        Nombre1 = pe.Nombre1,
                        FechaNacimiento = pe.FechaNacimiento,
                        Rut = pe.Rut
                    };

                    agregarPersona(pnew);
                }

            } 

           
        }

        public List<Persona> listarPersonas()
        {

            List<Persona> persona = new List<Persona>();

            try { 
           
                using (StreamReader reader = new StreamReader(ruta))
                {
                    String filas="";
                

                    do
                    {


                        filas = reader.ReadLine();
                        if (filas != null) { 
                            String[] textArr= filas.Split(";");
                            string nombre=textArr[0];
                            uint rut = Convert.ToUInt32(textArr[1]);
                            string fechNacimiento = textArr[2];
                            Int32 identificador = Convert.ToInt32(textArr[3]);

                            Persona p = new Persona() {
                                Nombre1=nombre,Rut=rut,FechaNacimiento=fechNacimiento,Identificador= identificador
                            };
                            persona.Add(p);
                            //Console.WriteLine(nombre);
                        }


                    } while (filas != null); 



                }
            }catch(Exception e)
            {
                Console.WriteLine(e);
            }


           
            

   



            return persona;

        }
    }
}
