﻿using CrudModel.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace CrudModel.DAL
{
    public interface IPersonaDAL
    {
        void agregarPersona(Persona p);
        List<Persona> listarPersonas();
        List<Persona> BuscarNombre(String nombre);
        void EliminarPersona(int index);
        void actualizarPersona(Persona p);

    }
}
