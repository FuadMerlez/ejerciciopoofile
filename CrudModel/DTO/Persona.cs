﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CrudModel.DTO
{
    public class Persona
    {
        private uint rut;
        private String Nombre;
        private String fechaNacimiento; //dd/mm/yyyy
        private int identificador;



        public uint Rut { get => rut; set => rut = value; }
        public string Nombre1 { get => Nombre; set => Nombre = value; }
        public string FechaNacimiento { get => fechaNacimiento; set => fechaNacimiento = value; }
        public int Identificador { get => identificador; set => identificador = value; }

        public int edadPersona() {
            int edad;
            DateTime fechaActual = DateTime.Today;
            edad = fechaActual.Year - DateTime.Parse(this.fechaNacimiento).Year;
            if (fechaActual.Month < DateTime.Parse(this.fechaNacimiento).Month) {
                --edad;
            }
            return edad;
        
        }



        public override string ToString()
        {
            return $"El usuario {this.Nombre}, de rut {this.rut}, nacio en {this.fechaNacimiento}, el usuario de id {this.identificador} y tiene {this.edadPersona()}";
        }




    }
}
