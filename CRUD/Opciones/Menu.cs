﻿using CrudModel.DAL;
using CrudModel.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRUD
{
    public partial class Menu{
        //private static List<Persona> arrPersonas = new List<Persona>();  //Vieja escuela

        //private static PersonaDalObjeto personaDal = new PersonaDalObjeto();
        private static PersonaDalArchivo personaDal = new PersonaDalArchivo();

        private static void actualizarPersona()
        {
            Console.Clear();
            Console.WriteLine("Listado de personas, Seleccione el ID de la persona a Modificar");
            personaDal.listarPersonas().ForEach(p => Console.WriteLine($"{p.Identificador}) nombre= {p.Nombre1}"));

            int index = Convert.ToInt32(Console.ReadLine());


            uint rut;
            String Nombre;
            String fechaNacimiento; //dd/mm/yyyy
            // uint identificador;

            Boolean esValido;

            do
            {
                Console.WriteLine("Ingrese rut sin - ");
                esValido = UInt32.TryParse(Console.ReadLine().Trim(), out rut);
            } while (!esValido);

            do
            {
                Console.WriteLine("Ingrese su Nombre");
                Nombre = Console.ReadLine().Trim();
            } while (Nombre == string.Empty);
            do
            {
                Console.WriteLine("Ingrese su Fecha de nacimiento");
                fechaNacimiento = Console.ReadLine().Trim();
            } while (fechaNacimiento == string.Empty);

            Persona p = new Persona();
            p.Nombre1 = Nombre;
            p.Rut = rut;
            p.FechaNacimiento = fechaNacimiento;
            p.Identificador = index;



            personaDal.actualizarPersona(p);

        }


        private static void elimiarPersona()
        {
            Console.WriteLine("Listado de personas, Seleccione el ID de la persona a eliminar");
             personaDal.listarPersonas().ForEach(p => Console.WriteLine($"{p.Identificador}) nombre= {p.Nombre1}"));

            personaDal.EliminarPersona(Convert.ToInt32(Console.ReadLine()));
        }

        private static void CrearPersona(){
            //RUT
            //NOMBRE
            //FECHA DE NACIMIENTO
            //IDENTIFICADOR
            Console.Clear();
            Console.WriteLine("Opcion agregar Persona!");

            uint rut;
            String Nombre;
            String fechaNacimiento; //dd/mm/yyyy
            // uint identificador;

            Boolean esValido;

            do {
                Console.WriteLine("Ingrese rut sin - ");
                esValido = UInt32.TryParse(Console.ReadLine().Trim(), out rut);
            } while (!esValido);

            do
            {
                Console.WriteLine("Ingrese su Nombre");
                Nombre = Console.ReadLine().Trim();
            } while (Nombre == string.Empty);
            do
            {
                Console.WriteLine("Ingrese su Fecha de nacimiento");
                fechaNacimiento = Console.ReadLine().Trim();
            } while (fechaNacimiento == string.Empty);

            Persona p = new Persona();
            p.Nombre1 = Nombre;
            p.Rut = rut;
            p.FechaNacimiento = fechaNacimiento;
            p.Identificador = personaDal.listarPersonas().Count + 1;
            personaDal.agregarPersona(p);
            
            
            //p.Identificador = identificador;
            
           /*p.Identificador = arrPersonas.Count + 1;
            
            arrPersonas.Add(p);

            for(int i=0; i<arrPersonas.Count; i++){
                Console.WriteLine(arrPersonas[i].Nombre1);
                Console.WriteLine(arrPersonas[i].ToString());   
            } */

        }

        public static void listarPersonas()
        {
            personaDal.listarPersonas().ForEach(p => Console.WriteLine($"{p.ToString()}"));
        }

    }
}
